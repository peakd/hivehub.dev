# HiveHub.dev

HiveHub.dev is a modular platform hosting multiple projects for the Hive blockchain ecosystem.

Current supported modules:

- [explorer](/src/explorer)
- [market](/src/market)
- [home](/src/home)

### Install

```
npm install -g pnpm

pnpm i
```

### Dev

```
pnpm run dev
```

### Color and style palette

#### Text

##### Light mode

text-gray-900 (#111827),
text-gray-700 (#374151)
text-gray-500 (#6b7280),

##### Dark mode

text-gray-100 (#111827),
text-gray-200 (#e5e7eb)
text-gray-300 (#6b7280),

#### Links

text-emerald-600 (#059669) dark:text-emerald-400 (#34d399)
font-semibold
font-medium

#### Header/footer with fixed dark background

text-white
text-slate-300 (#e2e8f0)

#### Containers/tables backgrounds

bg-white (#ffffff)
bg-gray-50 (#f9fafb)
border-gray-200 (#e5e7eb)
border-gray-300 (#d1d5db)

##### Dark mode

bg-slate-800 (#1e293b)
bg-slate-700 (#f9fafb)
border-slate-600 (#475569)
border-slate-500 (#e5e7eb)

#### Tables headers with sorting (Witnesses and Proposals)

bg-gray-50 (#f9fafb)
bg-gray-200 (#e5e7eb)
hover:bg-gray-100 (#111827)

##### Dark mode

bg-slate-800 (#1e293b)
bg-slate-600/50
bg-slate-700/50

#### Hover

bg-gray-50 (#f9fafb)
bg-gray-100 (#f3f4f6)

.button 1 (container) {
text-gray-700 (#374151)
bg-white (#ffffff)
hover:bg-gray-50 (#f9fafb)
border-gray-300 (#d1d5db)
grow text-center px-4 py-2 border shadow-sm text-sm font-medium rounded-md
}

.button 2 (modal) {
text-white (#ffffff)

#### Active/Disabled data

##### Active

bg-emerald-100 (100) dark:bg-emerald-700 (700)
text-emerald-700 (700) dark:text-emerald-100 (100)

##### Disabled

bg-red-100 (#fee2e2) dark:bg-red-700 (#b91c1c)
text-red-700 (#b91c1c) dark:text-red-100 (#fee2e2)

#### Custom colors (not Tailwind native and manually added in tailwind.config.js)

bg-bright-gradient (#0f766d)
hover:bg-dark-gradient (#1e293b)
