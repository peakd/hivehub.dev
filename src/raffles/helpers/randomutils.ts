import * as seedrandom from 'seedrandom'

/*
    Raffle supports the following formats
        csv:  `userA, userB` or weighted cvs 
              `userA 5, userB 0.5`
        line separated list:
              userA 10
              userB 20
        json: `["userA", "userB"]` or 
              `[["userA", 5], ["userB", 1.5]]`
              `[{name:"userA", points:1.2}]`
              `[{player:"userA", points: 1}]`    
*/
export function parseRaffle(obj) {
  if (typeof obj === 'string') {
    obj = obj.trim()
    if (obj.startsWith('[') && obj.endsWith(']')) obj = JSON.parse(obj)
  }
  var data = []
  if (typeof obj === 'string') {
    var entries = obj.trim().split(/[,\n]/)
    for (var entry of entries) {
      if (entry.trim().length === 0) continue
      var items = entry.trim().split(/[ ]+/)
      if (items.length !== 1 && items.length !== 2) throw 'Unsupported raffle format!'
      var name = items[0]
      var points = items.length === 2 ? Number(items[1]) : 1
      if (points % 1 !== 0) throw 'Unsupported raffle format!'
      data.push({ name, points })
    }
  } else if (Array.isArray(obj)) {
    for (var item of obj) {
      if (Array.isArray(item)) {
        if (item.length !== 1 && item.length !== 2) throw 'Unsupported raffle format!'
        var name = item[0]
        var points = item.length === 2 ? Number(item[1]) : 1
        data.push({ name, points })
      } else if (typeof item === 'object') {
        var name = item['name']
        if (name === undefined) name = item['player']
        var points = item['points']
        if (name === undefined) throw 'Unsupported raffle format!'
        points = points === undefined ? 1 : Number(points)
        data.push({ name, points })
      }
    }
  } else throw 'Unsupported raffle format!'
  return data
}
export async function raffleFrom(obj, seed, winners = 1, duplicateWinners = false): Promise<any[]> {
  return await raffle(parseRaffle(obj), seed, winners, duplicateWinners)
}
export async function raffle(leaderboard, seed, winners = 1, duplicateWinners = false): Promise<any[]> {
  var result = []
  if (leaderboard.length <= 0 || winners <= 0) return result
  const seedRng = seedrandom.default(seed)
  let totalTickets = 0
  const leaderboardWithTickets = []
  for (let player of leaderboard) {
    leaderboardWithTickets.push({
      ...player,
      startTicket: totalTickets + 1,
      endTicket: totalTickets + 1 + player.points,
      winner: false
    })
    totalTickets += player.points
  }
  const maxWinners = Math.min(winners, leaderboard.length)
  for (let i = 0; i < maxWinners; i++) {
    const remainingTickets = leaderboardWithTickets.filter(player => !player.winner).reduce((acc, player) => acc + player.points, 0)

    const roll = await seedRng()
    const ticketPosition = roll * remainingTickets + 1

    let ticket = null
    let selected = null
    let current = 0
    for (let player of leaderboardWithTickets) {
      if (player.winner) continue

      if (ticketPosition <= current + player.points + 1) {
        player.winner = duplicateWinners ? false : true
        ticket = player.startTicket + 1 + current + player.points - ticketPosition
        selected = {
          name: player.name,
          points: player.points,
          startTicket: player.startTicket,
          endTicket: player.endTicket,
          ticket
        }
        break
      }
      current += player.points
    }

    if (!selected) {
      console.error(`Winner not found, invalid ticket number: ${ticket}`)
      throw new Error(`Invalid ticket number: ${ticket}`)
    }

    result.push(selected)
  }
  return result
}
