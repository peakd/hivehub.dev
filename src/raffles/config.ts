import { RouteRecordRaw } from 'vue-router'

const name = 'raffles'

const baseNames = ['raffle', 'raffles']
const routes: Array<RouteRecordRaw> = baseNames.flatMap(baseName => [
  {
    path: `/${baseName}`,
    name: 'raffles',
    component: () => import('~/raffles/pages/Raffles.vue'),
    props: true,
    meta: { title: 'Raffle Validator' }
  },
  {
    path: `/${baseName}/:t([a-z0-9]{40})`,
    name: 'rafflesResult',
    component: () => import('~/raffles/pages/RaffleResult.vue'),
    props: true,
    meta: { title: 'Raffle Result: {t}' }
  }
])

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_RANDOM === 'false' ? false : true, // enabled by default
  routes: routes
}
