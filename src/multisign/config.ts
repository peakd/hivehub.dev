import { RouteRecordRaw } from 'vue-router'

const name = 'multisign'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/multisign',
    name: 'multisign',
    component: () => import('~/multisign/pages/Multisign.vue'),
    meta: { title: 'Create operation' }
  },
  {
    path: '/multisign/:encoded',
    name: 'sign',
    component: () => import('~/multisign/pages/Multisign.vue'),
    props: true,
    meta: { title: 'Sign operation' }
  },
  {
    path: '/multisign/setup_account/@:account([a-z][a-z0-9-.]{2,15})',
    name: 'set_up_multisign_account',
    component: () => import('~/multisign/components/SetMultisignAccount.vue'),
    beforeEnter: (to, from, next) => {
      if (to.params.account !== `${to.params.account}`.toLowerCase()) {
        next(to.fullPath.toLowerCase())
      }
      next()
    },
    props: true,
    meta: { title: 'Set up a multisign account' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_MULTISIGN === 'true' ? true : false, // disabled by default
  routes: routes
}
