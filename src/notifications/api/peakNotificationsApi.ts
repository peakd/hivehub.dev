import axios from 'axios'

const ENDPOINT = import.meta.env.VITE_APP_NOTIFICATIONS_ENDPOINT || 'https://notifications.hivehub.dev'

export const getConfig = async (account: string): Promise<any> => {
  return (await axios.get(`${ENDPOINT}/users/${account}`)).data
}

export const getNotifications = async (account: string, limit: number = 100, offset: number = 0): Promise<any> => {
  return (await axios.get(`${ENDPOINT}/notifications/${account}`)).data
}
