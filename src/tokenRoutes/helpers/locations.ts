import heIcon from '~/assets/images/icons/hive-engine.svg'
import hiveIcon from '~/assets/images/icons/hive.svg'
import dCropsIcon from '~/assets/images/icons/dcrops.svg'
import beeswapIcon from '~/assets/images/icons/beeswap.png'
import leodexIcon from '~/assets/images/icons/leodex.png'
import binanceIcon from '~/assets/images/icons/binance.svg'
import geminiIcon from '~/assets/images/icons/gemini.svg'
import ltcIcon from '~/assets/images/icons/ltc.svg'
import waxIcon from '~/assets/images/icons/wax.svg'
import dogeIcon from '~/assets/images/icons/doge.svg'
import btcIcon from '~/assets/images/icons/bitcoin.svg'
import eosIcon from '~/assets/images/icons/eos.svg'
import maticIcon from '~/assets/images/icons/matic.svg'
import bscIcon from '~/assets/images/icons/bsc.svg'
import ethIcon from '~/assets/images/icons/eth.svg'
import slIcon from '~/assets/images/icons/splinterlands.png'
import krakenIcon from '~/assets/images/icons/kraken.svg'
import gateIcon from '~/assets/images/icons/gate.svg'
import binanceUsIcon from '~/assets/images/icons/binanceus.svg'
import upbitIcon from '~/assets/images/icons/upbit.svg'
import mexcIcon from '~/assets/images/icons/mexc.svg'
import simpleswapIcon from '~/assets/images/icons/simpleswap.svg'
import coinbaseIcon from '~/assets/images/icons/coinbase.svg'
import keychainIcon from '~/assets/images/icons/keychain.svg'
import uswapIcon from '~/assets/images/icons/uswap.png'
import htxIcon from '~/assets/images/icons/htx.png'

export const locationsAndServices: any = {
  locations: {
    hive: { type: 'CHAIN', icon: hiveIcon },
    'hive-engine': { type: 'DEX', icon: heIcon },
    binance: { type: 'CEX', icon: binanceIcon },
    gemini: { type: 'CEX', icon: geminiIcon },
    wax: { type: 'CHAIN', icon: waxIcon },
    ltc: { type: 'CHAIN', icon: ltcIcon },
    eos: { type: 'CHAIN', icon: eosIcon },
    btc: { type: 'CHAIN', icon: btcIcon },
    bsc: { type: 'CHAIN', icon: bscIcon },
    matic: { type: 'CHAIN', icon: maticIcon },
    eth: { type: 'CHAIN', icon: ethIcon },
    doge: { type: 'CHAIN', icon: dogeIcon },
    splinterlands: { type: 'CHAIN', icon: slIcon },
    kraken: { type: 'CEX', icon: krakenIcon },
    gate: { type: 'CEX', icon: gateIcon },
    binanceus: { type: 'CEX', icon: binanceUsIcon },
    upbit: { type: 'CEX', icon: upbitIcon },
    mexc: { type: 'CEX', icon: mexcIcon },
    coinbase: { type: 'CEX', icon: coinbaseIcon },
    htx: { type: 'CEX', icon: htxIcon }
  },
  services: {
    cropswap: { type: 'bridge', icon: dCropsIcon },
    beeswap: { type: 'bridge', icon: beeswapIcon },
    leodex: { type: 'bridge', icon: leodexIcon },
    keychain: { type: 'bridge', icon: keychainIcon },
    uswap: { type: 'bridge', icon: uswapIcon },
    simpleswap: { type: 'swap', icon: simpleswapIcon }
  }
}

export const locationsIcon: any = {
  hive: hiveIcon,
  'hive-engine': heIcon,
  cropswap: dCropsIcon,
  beeswap: beeswapIcon,
  leodex: leodexIcon,
  binance: binanceIcon,
  gemini: geminiIcon,
  wax: waxIcon,
  ltc: ltcIcon,
  eos: eosIcon,
  btc: btcIcon,
  bsc: bscIcon,
  matic: maticIcon,
  eth: ethIcon,
  doge: dogeIcon,
  splinterlands: slIcon,
  kraken: krakenIcon,
  gate: gateIcon,
  binanceus: binanceUsIcon,
  upbit: upbitIcon,
  mexc: mexcIcon,
  simpleswap: simpleswapIcon,
  coinbase: coinbaseIcon,
  keychain: keychainIcon,
  uswap: uswapIcon,
  htx: htxIcon
}
