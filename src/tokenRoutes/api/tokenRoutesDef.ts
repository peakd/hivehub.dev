/*
THIS DOCUMENT CONTAINS THE DEFINITION FOR AN INTERFACE FOR COMMUNITIES
DEFINITION THAT WILL BE USED IN THE "TOKEN ROUTES" PROJECT
*/

export interface Token{
    name: string;
    symbol: string;
    price: number;
    website: string;
}