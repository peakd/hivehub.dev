export type Token = 'HIVE' | 'HBD'

export type Order = {
  op_id: string
  current_orderid: string
  current_owner: string
  current_pays_symbol: Token
  current_pays: string
  open_orderid: string
  open_owner: string
  open_pays_symbol: Token
  open_pays: string
  timestamp: string
}

export type MergedOrder = {
  orderid: string
  type: 'buy' | 'sell'
  hive: number
  hbd: number
  timestamp: string
  details: Order[]
}
