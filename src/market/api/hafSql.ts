import axios from 'axios'
import { Order } from '../typeDef'

const endpoint = 'https://rpc.mahdiyari.info'

export const getFillOrders = async (username: string, type: 'open' | 'current'): Promise<Order[]> => {
  try {
    const payload = {
      jsonrpc: '2.0',
      method: 'hafsql.vo_fill_order',
      params: {
        limit: 100
      },
      id: 1
    }

    if (type === 'current') {
      payload.params.current_owner = username
    } else {
      payload.params.open_owner = username
    }

    const response = await axios.post(endpoint, payload)

    return response.data.result
  } catch (err) {
    console.error('Error fetching data:', err)
    return []
  }
}
