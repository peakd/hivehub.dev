import { RouteRecordRaw } from 'vue-router'
import { useSettingsStore } from '~/stores/settings'

const name = 'market'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/market',
    redirect: to => {
      const settingsStore = useSettingsStore()
      if (settingsStore?.settings?.marketView) {
        return { path: `/market/${settingsStore.settings.marketView}` }
      } else {
        return { path: `/market/swap` }
      }
    }
  },
  {
    path: '/market/swap',
    name: 'swap',
    component: () => import('~/market/pages/Market.vue'),
    props: { default: true, view: 'swap' }
  },
  {
    path: '/market/:view(basic|limit)',
    name: 'basic',
    component: () => import('~/market/pages/Market.vue'),
    props: { default: true, view: 'basic' },
    meta: { expand: true }
  },
  {
    path: '/market/advanced',
    name: 'advanced',
    component: () => import('~/market/pages/Market.vue'),
    props: { default: true, view: 'advanced' },
    meta: { expand: true }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_MARKET === 'false' ? false : true, // enabled by default
  routes: routes
}
