/*
THIS DOCUMENT CONTAINS THE DEFINITION FOR AN INTERFACE FOR COMMUNITIES
DEFINITION THAT WILL BE USED IN THE "POLLS" PROJECT
*/

export interface Poll {
  post_title: string
  post_body: string
  author: string
  created: string
  permlink: string
  parent_permlink: string
  parent_author?: string
  tags: string[]
  image: string[]
  protocol_version: number
  question: string
  preferred_interpretation: 'number_of_votes' | 'tokens'
  token?: string | null
  end_time: string | null
  status: 'Active' | 'Ended'
  max_choices_voted: number
  filter_account_age_days: number
  ui_hide_res_until_voted?: boolean | null
  allow_vote_changes: boolean
  platform?: string | null
  poll_trx_id: string
  poll_choices: PollChoice[]
  poll_stats: PollStats
  voted_by_user?: boolean
}

interface PollChoice {
  choice_num: number
  choice_text: string
  votes: PollVotes | null
}

interface PollVotes {
  total_votes: number
  hive_hp: number
  hive_proxied_hp: number
  hive_hp_incl_proxied: number
  spl_spsp: number
  he_token: string | null
}

interface PollStats {
  total_voting_accounts_num: number
  total_hive_hp: number
  total_hive_proxied_hp: number
  total_hive_hp_incl_proxied: number
  total_spl_spsp: number
  total_he_token: string | null
}
