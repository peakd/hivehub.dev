import axios from 'axios'

const useBeta = false
const ENDPOINT = import.meta.env.VITE_APP_STATS_ENDPOINT || 'https://polls.hivehub.dev/rpc'
const BETA_ENDPOINT = 'https://polls-beta.hivehub.dev/rpc'

const endpoint = useBeta ? BETA_ENDPOINT : ENDPOINT

export const fetchPolls = async (offset: number, val?: string | null, limit: number = 100): Promise<any> => {
  let result = null

  if (val === 'active' || val === 'ended') {
    const type = val.charAt(0).toUpperCase() + val.slice(1)
    result = await axios.get(`${endpoint}/polls?status=eq.${type}&limit=${limit}&offset=${offset}`)
  } else {
    result = await axios.get(`${endpoint}/polls?limit=${limit}&offset=${offset}`)
  }

  return result.data
}

export const fetchIndividualPoll = async (author: string, permlink: string): Promise<any> => {
  const result = await axios.get(`${endpoint}/poll?author=eq.${author}&permlink=eq.${permlink}`)
  return result.data
}

export const fetchPollsVotedByUser = async (user: string): Promise<any> => {
  const result = await axios.get(`${endpoint}/poll?poll_voters=cs.[{"name":"${user}"}]&order=created.desc`)
  return result.data
}
