import { RouteRecordRaw } from 'vue-router'
import { fetchIndividualPoll } from './api/pollApi'

const name = 'polls'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/polls',
    name: 'polls',
    component: () => import('~/polls/pages/Polls.vue'),
    props: true,
    meta: { title: 'Polls' }
  },
  {
    path: '/poll/@:author([a-z][a-z0-9-.]{2,15})/:permlink',
    name: 'poll',
    props: true,
    component: () => import('~/polls/pages/SpecificPoll.vue'),
    meta: { title: 'New Poll' },
    async beforeEnter(to: any) {
      try {
        const response = await fetchIndividualPoll(to.params.author, to.params.permlink)
        if (response instanceof Error) {
          throw new Error('Failed to fetch poll data')
        }
        to.meta.title = `${response[0].post_title}`
      } catch (error) {
        console.error(error)
        throw new Error('Failed to fetch poll data')
      }
    }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_POLLS === 'false' ? false : true, // disabled by default
  routes: routes
}
