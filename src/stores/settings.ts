import { acceptHMRUpdate, defineStore } from 'pinia'
import { Ref } from 'vue'
import { getApiNodes, updateHiveEndpoint } from '~/common/api/hiveApi'
import { getHiveEngineApiNodes, updateHiveEngineEndpoint } from '~/explorer/api/hiveEngineApi'
import { setBestNode } from '~/common/helpers/hiveFunctions'

type AppSettings = {
  apiNode: string
  autoApiNode: boolean
  heApiNode: string
  autoHeApiNode: boolean
  language: string
  darkMode: boolean
  transactionsRendering: boolean
  timestampRendering: string
  jsonRendering: boolean
  jsonFontSize?: boolean
  marketView?: string
}

const defaultSettings: AppSettings = {
  apiNode: 'https://api.hive.blog',
  autoApiNode: false,
  heApiNode: 'https://api.hive-engine.com/rpc/',
  autoHeApiNode: false,
  language: 'en',
  darkMode: false,
  transactionsRendering: false,
  timestampRendering: 'relative',
  jsonRendering: true,
  jsonFontSize: false,
  marketView: 'swap'
}

export const useSettingsStore = defineStore('settings', () => {
  const { locale } = useI18n()

  let localStorageJson = {}

  const localStorageItem = localStorage.getItem('settings')
  if (localStorageItem) {
    try {
      localStorageJson = JSON.parse(localStorageItem)
    } catch (error) {
      console.error('Error parsing stored settings', error)
    }
  }

  const propagateSettings = async (newSettings: AppSettings) => {
    if (newSettings.autoApiNode || !localStorageItem) {
      const hiveNodes = await getApiNodes()
      const bestNode = setBestNode(newSettings.apiNode, hiveNodes, false)
      newSettings.apiNode = bestNode.endpoint
    }
    updateHiveEndpoint(newSettings.apiNode)

    if (newSettings.autoHeApiNode || !localStorageItem) {
      const hiveEngineNodes = await getHiveEngineApiNodes()
      const bestHeNode = setBestNode(newSettings.heApiNode, hiveEngineNodes, true)
      newSettings.heApiNode = bestHeNode.endpoint
    }
    updateHiveEngineEndpoint(newSettings.heApiNode)

    if (newSettings.darkMode) {
      if (!document.documentElement.classList.contains('dark')) {
        document.documentElement.classList.add('dark')
      }
    } else {
      document.documentElement.classList.remove('dark')
    }

    if (newSettings.language) {
      locale.value = newSettings.language
    }
  }

  const settings: Ref<AppSettings> = ref({
    ...defaultSettings,
    ...localStorageJson
  })

  if (settings.value.apiNode) {
    updateHiveEndpoint(settings.value.apiNode)
  }
  if (settings.value.heApiNode) {
    updateHiveEngineEndpoint(settings.value.heApiNode)
  }

  propagateSettings(settings.value)

  const saveSettings = async (updatedSettings: AppSettings) => {
    propagateSettings(updatedSettings)

    settings.value = updatedSettings
    localStorage.setItem('settings', JSON.stringify(updatedSettings))
  }

  return {
    settings,
    saveSettings
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useSettingsStore, import.meta.hot))
