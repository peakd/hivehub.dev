import { acceptHMRUpdate, defineStore } from 'pinia'
import { Ref } from 'vue'
import { getOrderBook, getTradeHistory } from '~/common/api/hiveApi'

type MarketData = {
  HIVEHBD: number | null
  HBDHIVE: number | null
  HIVEAmount: number
  HBDAmount: number
  HIVECost: number
  HBDCost: number
  tradeHistory: { date: string; current_pays: string; open_pays: string }[]
  marketStats: { hiveUsd: string | null; hiveUsdSource: string | null; hbdUsd: string | null; hbdUsdSource: string | null }
  orderBook: {
    asks: { created: string; hive: number; hbd: number; order_price: { base: string; quote: string }; real_price: string }[]
    bids: { created: string; hive: number; hbd: number; order_price: { base: string; quote: string }; real_price: string }[]
  }
  toggleBuyHIVE: boolean
}

export const useMarketStore = defineStore('market', () => {
  const market: Ref<MarketData> = ref({
    HIVEHBD: null,
    HBDHIVE: null,
    HIVEAmount: 0,
    HBDAmount: 0,
    HIVECost: 0,
    HBDCost: 0,
    tradeHistory: [],
    marketStats: { hiveUsd: '', hiveUsdSource: 'Coingecko (HIVE)', hbdUsd: '', hbdUsdSource: 'Coingecko (HIVE)' },
    orderBook: { asks: [], bids: [] },
    toggleBuyHIVE: false
  })

  const setHIVEHBD = (ratio: number) => {
    market.value.HIVEHBD = parseFloat(parseFloat(ratio).toFixed(4))
    market.value.HBDHIVE = parseFloat(parseFloat(1 / ratio).toFixed(4))

    market.value.HIVEAmount = parseFloat(parseFloat(market.value.HBDCost / (market.value.HIVEHBD || 0)).toFixed(3))
    market.value.HBDAmount = parseFloat(parseFloat(market.value.HIVECost / (market.value.HBDHIVE || 0)).toFixed(3))
  }
  const setHBDHIVE = (ratio: number) => {
    market.value.HBDHIVE = parseFloat(parseFloat(ratio).toFixed(4))
    market.value.HIVEHBD = parseFloat(parseFloat(1 / ratio).toFixed(4))

    market.value.HBDAmount = parseFloat(parseFloat(market.value.HIVECost / (market.value.HBDHIVE || 0)).toFixed(3))
    market.value.HIVEAmount = parseFloat(parseFloat(market.value.HBDCost / (market.value.HIVEHBD || 0)).toFixed(3))
  }
  const setHIVEAmount = () => {
    market.value.HIVEAmount = parseFloat(parseFloat(market.value.HBDCost / (market.value.HIVEHBD || 0)).toFixed(4))
  }
  const setHBDAmount = () => {
    market.value.HBDAmount = parseFloat(parseFloat(market.value.HIVECost / (market.value.HBDHIVE || 0)).toFixed(4))
  }
  const setHIVECost = () => {
    if (!market.value.HBDHIVE && market.value.HIVECost && market.value.HBDAmount) {
      market.value.HBDHIVE = parseFloat(market.value.HIVECost) / parseFloat(market.value.HBDAmount)
    }
    market.value.HIVECost = parseFloat(parseFloat(market.value.HBDAmount * (market.value.HBDHIVE || 0)).toFixed(4))
  }
  const setHBDCost = () => {
    if (!market.value.HIVEHBD && market.value.HIVEAmount && market.value.HBDCost) {
      market.value.HIVEHBD = parseFloat(market.value.HBDCost) / parseFloat(market.value.HIVEAmount)
    }
    market.value.HBDCost = parseFloat(parseFloat(market.value.HIVEAmount * (market.value.HIVEHBD || 0)).toFixed(4))
  }

  const updateOrderBook = async () => {
    market.value.orderBook = await getOrderBook(100)
    return market.value.orderBook
  }

  const updateTradeHistory = async () => {
    const today = new Date()
    const date = new Date(new Date().setHours(today.getHours() - 3))
    const yesterday =
      date.getUTCFullYear() +
      '-' +
      (date.getUTCMonth() + 1) +
      '-' +
      date.getUTCDate() +
      'T' +
      date.getUTCHours() +
      ':' +
      date.getUTCMinutes() +
      ':' +
      date.getUTCSeconds()
    market.value.tradeHistory = await getTradeHistory(1000, yesterday)
    return market.value.tradeHistory
  }

  const onToggleBuyHIVE = () => {
    market.value.toggleBuyHIVE = !market.value.toggleBuyHIVE
  }

  return {
    setHIVEHBD,
    setHBDHIVE,
    setHIVEAmount,
    setHBDAmount,
    setHIVECost,
    setHBDCost,
    updateOrderBook,
    updateTradeHistory,
    market,
    onToggleBuyHIVE
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useMarketStore, import.meta.hot))
