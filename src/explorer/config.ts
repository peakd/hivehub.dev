import { RouteRecordRaw } from 'vue-router'

const name = 'explorer'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/blocks',
    name: 'blocks',
    component: () => import('~/explorer/pages/Blocks.vue'),
    meta: { title: 'Recent Blocks' }
  },
  {
    path: '/b/:block(\\d+)',
    name: 'block',
    component: () => import('~/explorer/pages/Block.vue'),
    props: true,
    meta: { title: 'Block: {block}' }
  },
  {
    path: '/tx/:txId([a-z0-9]{40})',
    name: 'transaction',
    component: () => import('~/explorer/pages/Transaction.vue'),
    props: true,
    meta: { title: 'Tx: {txId}' }
  },
  {
    path: '/tx/:block/:txId([a-z0-9]{40})/:opInTx',
    name: 'virtual_transaction',
    component: () => import('~/explorer/pages/VirtualTransaction.vue'),
    props: true,
    meta: { title: 'Block: {block}, Virtual Tx: {txId}/{opInTx}' }
  },
  {
    path: '/@:account([a-z][a-z0-9-.]{2,15})',
    name: 'account',
    component: () => import('~/explorer/pages/Account.vue'),
    beforeEnter: (to, from, next) => {
      if (to.params.account !== `${to.params.account}`.toLowerCase()) {
        next(to.fullPath.toLowerCase())
      }
      next()
    },
    props: true
  },
  {
    path: '/raw/@:account([a-z][a-z0-9-.]{2,15})',
    name: 'raw_account',
    component: () => import('~/explorer/pages/AccountRawData.vue'),
    props: true,
    meta: { title: '@{account} raw data' }
  },
  {
    path: '/@:author([a-z][a-z0-9-.]{2,15})/:permlink',
    name: 'post',
    component: () => import('~/explorer/pages/Post.vue'),
    props: true
  },
  {
    path: '/:parent/@:author([a-z][a-z0-9-.]{2,15})/:permlink',
    redirect: to => ({ name: 'post', params: { author: to.params.author, permlink: to.params.permlink } })
  },
  {
    path: '/witnesses',
    name: 'witnesses',
    component: () => import('~/explorer/pages/Witnesses.vue'),
    props: true,
    meta: { title: 'Witnesses' }
  },
  {
    path: '/witnesses/@:witness([a-z][a-z0-9-.]{2,15})',
    name: 'witness',
    component: () => import('~/explorer/pages/WitnessStats.vue'),
    props: true,
    meta: { title: 'Witness: @{witness}' }
  },
  {
    path: '/witnesses/hive-engine',
    name: 'witnesses-hive-engine',
    component: () => import('~/explorer/pages/WitnessesHiveEngine.vue'),
    props: true,
    meta: { title: 'Hive Engine Witnesses' }
  },
  {
    path: '/proposals',
    name: 'proposals',
    component: () => import('~/explorer/pages/Proposals.vue'),
    props: true,
    meta: { title: 'Proposals' }
  },
  {
    path: '/proposals/:proposalId([0-9]{1,4})',
    name: 'proposal',
    component: () => import('~/explorer/pages/ProposalStats.vue'),
    props: true,
    meta: { title: 'Proposal #{proposalId}' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_EXPLORER === 'false' ? false : true, // enabled by default
  routes: routes
}
