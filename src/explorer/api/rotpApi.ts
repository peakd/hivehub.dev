import axios from 'axios'

const ROTP_BETA_ENDPOINT = import.meta.env.VITE_APP_GLS_ENDPOINT || 'https://beta-api.riseofthepixels.com'
const ROTP_ENDPOINT = import.meta.env.VITE_APP_GLS_ENDPOINT || 'https://api.riseofthepixels.com'

export const getBetaTransaction = async (trxId: string): Promise<any> => {
  return (await axios.get(`${ROTP_BETA_ENDPOINT}/transactions/tx/${trxId}`)).data
}

export const getTransaction = async (trxId: string): Promise<any> => {
  return (await axios.get(`${ROTP_ENDPOINT}/transactions/tx/${trxId}`)).data
}
