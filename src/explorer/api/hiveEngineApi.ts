import axios from 'axios'
import SSC from '@hive-engine/sscjs'

const SSC_ENDPOINT = import.meta.env.VITE_APP_SSC_ENDPOINT || 'https://api.hive-engine.com/rpc/'
let ssc = new SSC(SSC_ENDPOINT)

export const getTransaction = async (trxId: string): Promise<any> => {
  return await ssc.getTransactionInfo(trxId)
}

export const getWitnesses = async (): Promise<any[]> => {
  return await ssc.find('witnesses', 'witnesses', {}, 1000, 0, [{ index: 'approvalWeight', descending: true }])
}

export const getWitnessParams = async (): Promise<any> => {
  return await ssc.findOne('witnesses', 'params', {})
}

export const getHiveEngineApiNodes = async (): Promise<any> => {
  const response = await axios.get('https://beacon.peakd.com/api/he/nodes')
  return response.data
}

export const updateHiveEngineEndpoint = (endpoint: string) => {
  const normalizedEndpoint = ['https://api.hive-engine.com', 'https://api2.hive-engine.com'].includes(endpoint) ? `${endpoint}/rpc/` : endpoint
  ssc = new SSC(normalizedEndpoint)
}
