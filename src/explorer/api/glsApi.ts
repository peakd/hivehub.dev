import axios from 'axios'

const GLS_ENDPOINT = import.meta.env.VITE_APP_GLS_ENDPOINT || 'https://validator.genesisleaguesports.com'

export const getTransaction = async (trxId: string): Promise<any> => {
  return (await axios.get(`${GLS_ENDPOINT}/transaction/${trxId}`)).data
}
