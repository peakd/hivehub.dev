import axios from 'axios'

const ENDPOINT = import.meta.env.VITE_APP_STATS_ENDPOINT || 'https://stats.hivehub.dev'

export const fetchData = async (metric: string, limit: number = 100, offset: number = 0): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${metric}`, { params: { limit, offset } })
    return response.data
  } catch (error) {
    console.error(`Error fetching data for metric ${metric}:`, error)
    throw error
  }
}

export const getCommunities = async (limit: number = 200, offset: number = 0): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/communities`, { params: { limit, offset } })
    return response.data
  } catch (error) {
    console.error('Error fetching communities:', error)
    throw error
  }
}

export const getCommunitiesByDays = async (
  limit: number = 200,
  offset: number = 0,
  days: number = 0,
  order_by: string = 'd_total_subscribers'
): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/communities`, { params: { limit, offset, days, order_by } })
    return response.data
  } catch (error) {
    console.error('Error fetching communities by days:', error)
    throw error
  }
}

export const getCommunitiesByOrder = async (limit: number = 300, offset: number = 0, order_by: string = 'd_total_subscribers'): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/communities`, { params: { limit, offset, order_by } })
    return response.data
  } catch (error) {
    console.error('Error fetching communities by order:', error)
    throw error
  }
}

export const getCommunity = async (c: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/communities`, { params: { c } })
    return response.data
  } catch (error) {
    console.error(`Error fetching community ${c}:`, error)
    throw error
  }
}

export const getCommunityByDays = async (c: string, d: number): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/communities`, { params: { c, d } })
    return response.data
  } catch (error) {
    console.error(`Error fetching community ${c} for days ${d}:`, error)
    throw error
  }
}

export const getComments = async (c: string, u: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${c}/comments/`, { params: { c, u } })
    return response.data
  } catch (error) {
    console.error(`Error fetching comments for community ${c} and user ${u}:`, error)
    throw error
  }
}

export const getPosts = async (c: string, u: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${c}/posts/`, { params: { c, u } })
    return response.data
  } catch (error) {
    console.error(`Error fetching posts for community ${c} and user ${u}:`, error)
    throw error
  }
}

export const getSubscribers = async (c: string, u: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${c}/subscribers_change/${u}`)
    return response.data
  } catch (error) {
    console.error(`Error fetching subscribers change for community ${c} and user ${u}:`, error)
    throw error
  }
}

export const getBadgesByOrder = async (limit: number = 200, offset: number = 0, order_by: string = 'd_num_recipients_active_last_30_days'): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/badges`, { params: { limit, offset, order_by } })
    return response.data
  } catch (error) {
    console.error('Error fetching badges by order:', error)
    throw error
  }
}

export const getBadgeNumOfRecipients = async (badgeId: string, timeUnit: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${badgeId}/num_recipients/${timeUnit}`)
    return response.data
  } catch (error) {
    console.error(`Error fetching number of recipients for badge ${badgeId}:`, error)
    throw error
  }
}

export const getBadgeNumOfActiveRecipients = async (badgeId: string, timeUnit: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${badgeId}/num_recipients_active_last_30_days/${timeUnit}`)
    return response.data
  } catch (error) {
    console.error(`Error fetching active recipients for badge ${badgeId} in the last 30 days:`, error)
    throw error
  }
}

export const getBadgeNumOfFollowers = async (badgeId: string, timeUnit: string): Promise<any> => {
  try {
    const response = await axios.get(`${ENDPOINT}/${badgeId}/num_followers/${timeUnit}`)
    return response.data
  } catch (error) {
    console.error(`Error fetching followers for badge ${badgeId}:`, error)
    throw error
  }
}
