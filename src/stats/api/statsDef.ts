/*
THIS DOCUMENT CONTAINS THE DEFINITION FOR AN INTERFACE FOR COMMUNITIES
DEFINITION THAT WILL BE USED IN THE "STATS" PROJECT
*/

export interface Community {
  name: string
  type: number
  title: string
  description: string
  about: string
  language: string
  nsfw: boolean
  total_subscribers: number
  total_posts: number
  total_comments: number
  average_daily_unique_post_authors: number
  average_daily_unique_comment_authors: number
  post_payouts_hbd: number
  comment_payouts_hbd: number
  total_payouts_hbd: number
}

export interface Badge {
  name: string
  created: string
  created_by: string
  date_of_last_recipient: string | null
  num_recipients_active_last_30_days: string
  num_recipients: string
  num_followers: string
  code: string
  profile_name: string | null
  about: string | null
  profile_image: string
  website: string
  approved?: boolean
}

export type BadgesSortValues = 'name' | 'num_recipients_active_last_30_days' | 'num_recipients' | 'num_followers' | 'created' | 'date_of_last_recipient'
