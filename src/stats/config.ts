import { RouteRecordRaw } from 'vue-router'

const name = 'stats'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/stats',
    name: 'stats',
    component: () => import('~/stats/pages/Stats.vue')
  },
  {
    path: '/stats/communities',
    name: 'communities',
    component: () => import('~/stats/pages/Communities.vue'),
    props: true,
    meta: { title: 'Communities (Lifetime Data)' }
  },
  {
    path: '/stats/communities/lastday',
    name: 'communities-daily',
    props: true,
    component: () => import('~/stats/pages/CommunitiesDaily.vue'),
    meta: { title: 'Communities (Last Day Data)' }
  },
  {
    path: '/stats/communities/last7days',
    name: 'communities-weekly',
    props: true,
    component: () => import('~/stats/pages/CommunitiesWeekly.vue'),
    meta: { title: 'Communities (Last 7 Days Data)' }
  },
  {
    path: '/stats/communities/last30days',
    name: 'communities-monthly',
    props: true,
    component: () => import('~/stats/pages/CommunitiesMonthly.vue'),
    meta: { title: 'Communities (Last 30 Days Data)' }
  },
  {
    path: '/c/:community([a-z][a-z0-9-.]{2,15})',
    name: 'community',
    props: true,
    component: () => import('~/stats/pages/Community.vue')
  },
  {
    path: '/stats/badges',
    name: 'badges',
    component: () => import('~/stats/pages/Badges.vue'),
    props: true,
    meta: { title: 'Stats for Badges', desc: '(data is uploaded once a day)' }
  },
  {
    path: '/b/:badge(badge-\\d{6})',
    name: 'badge',
    props: true,
    component: () => import('~/stats/pages/Badge.vue')
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_STATS === 'false' ? false : true, // disabled by default
  routes: routes
}
